package org.bitbucket.fredgrott.gwslae;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * <code>
 * <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
 *   <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
 *       <level>INFO</level>
 *   </filter>
 *   <filter class="com.myapp.ThresholdLoggerFilter">
 *       <logger>org.springframework</logger>
 *      <level>ERROR</level>
 *   </filter>
 *  </appender>
 * </code>
 * @author fredgrott
 *
 */
public class GWSLAEThresholdLoggerFilter extends Filter<ILoggingEvent> {

	/** The level. */
    private Level level;
    
    /** The logger. */
    private String logger;

    /**
     * Decide.
     *
     * @param event the event
     * @return the filter reply
     * @see ch.qos.logback.core.filter.Filter#decide(java.lang.Object)
     */
    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (!isStarted()) {
            return FilterReply.NEUTRAL;
        }

        if (!event.getLoggerName().startsWith(logger))
            return FilterReply.NEUTRAL;

        if (event.getLevel().isGreaterOrEqual(level)) {
            return FilterReply.NEUTRAL;
        } else {
            return FilterReply.DENY;
        }
    }

    /**
     * Sets the level.
     *
     * @param level the new level
     */
    public void setLevel(Level level) {
        this.level = level;
    }

    /**
     * Sets the logger.
     *
     * @param logger the new logger
     */
    public void setLogger(String logger) {
        this.logger = logger;
    }

    /**
     * Start.
     *
     * @see ch.qos.logback.core.filter.Filter#start()
     */
    public void start() {
        if (this.level != null && this.logger != null) {
            super.start();
        }
    }
	
	
}
