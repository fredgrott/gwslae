/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package org.bitbucket.fredgrott.gwslae;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * GWSLAENanoConverter, config xml file like this:
 * 
 * <code>
 * <configuration>
 *
 *	<conversionRule conversionWord="nano" 
 *                 converterClass="chapters.layouts.MySampleConverter" />
 *
 * <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
 *   <encoder>
 *     <pattern>%-6nano [%thread] %level - %msg%n</pattern>
 *   </encoder>
 * </appender>
 *
 * <root level="DEBUG">
 *  <appender-ref ref="STDOUT" />
 *  </root>
 * </configuration>
 * </code>
 * 
 * @author fredgrott
 *
 */
public class GWSLAENanoConverter extends ClassicConverter {

	long start = System.nanoTime();

	  @Override
	  public String convert(ILoggingEvent event) {
	    long nowInNanos = System.nanoTime();
	    return Long.toString(nowInNanos-start);
	  }
}
