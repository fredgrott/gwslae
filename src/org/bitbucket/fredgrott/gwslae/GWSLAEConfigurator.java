/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package org.bitbucket.fredgrott.gwslae;

import java.util.List;
import java.util.Map;

import ch.qos.logback.core.joran.GenericConfigurator;
import ch.qos.logback.core.joran.action.Action;
import ch.qos.logback.core.joran.action.ImplicitAction;
import ch.qos.logback.core.joran.spi.Interpreter;
import ch.qos.logback.core.joran.spi.Pattern;
import ch.qos.logback.core.joran.spi.RuleStore;

public class GWSLAEConfigurator extends GenericConfigurator {

	  final Map<Pattern, Action> ruleMap;
	  final List<ImplicitAction> iaList;

	  public GWSLAEConfigurator(Map<Pattern, Action> ruleMap) {
	    this(ruleMap, null);
	  }

	  public GWSLAEConfigurator(Map<Pattern, Action> ruleMap, List<ImplicitAction> iaList) {
	    this.ruleMap = ruleMap;
	    this.iaList = iaList;
	  }

	  @Override
	  protected void addInstanceRules(RuleStore rs) {
	    for (Pattern pattern : ruleMap.keySet()) {
	      Action action = ruleMap.get(pattern);
	      rs.addRule(pattern, action);
	    }
	  }

	  @Override
	  protected void addImplicitRules(Interpreter interpreter) {
	    if(iaList == null) {
	      return;
	    }
	    for (ImplicitAction ia : iaList) {
	      interpreter.addImplicitAction(ia);
	    }
	  }
}
