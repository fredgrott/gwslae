/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package org.bitbucket.fredgrott.gwslae;

import ch.qos.logback.core.boolex.EvaluationException;
import ch.qos.logback.core.boolex.EventEvaluator;
import ch.qos.logback.core.spi.ContextAwareBase;

/**
 * A simple EventEvaluator implementation that triggers email transmission after
 * 1024 events regardless of event level.
 * 
 * Obviously this gets a configuration file entry to trigger.
 * Replace GEEvaltuor with this one as:
 * <code>
 * <configuration>
 * <statusListener class="ch.qos.logback.core.status.OnConsoleStatusListener" />  
 *
 * <appender name="EMAIL" class="ch.qos.logback.classic.net.SMTPAppender">
 *  <evaluator class="ch.qos.logback.classic.boolex.GEventEvaluator">
 *     <expression>
 *       e.marker?.contains("NOTIFY_ADMIN") || e.marker?.contains("TRANSACTION_FAILURE")
 *     </expression>
 *   </evaluator>
 *   <SMTPHost>${smtpHost}</SMTPHost>
 *   <To>${to}</To>
 *   <From>${from}</From>
 *   <layout class="ch.qos.logback.classic.html.HTMLLayout"/>
 * </appender>
 *
 * <root>
 *   <level value ="debug"/>
 *   <appender-ref ref="EMAIL" />
 * </root>  
 * </configuration>
 * </code>
 * 
 */
public class GWSLAEEmailCounterBasedEvaluator extends ContextAwareBase implements EventEvaluator {

	static int LIMIT = 1024;
	  int counter = 0;
	  String name;
	  boolean started;

	  public boolean evaluate(Object event) throws NullPointerException,
	      EvaluationException {
	    counter++;

	    if (counter == LIMIT) {
	      counter = 0;

	      return true;
	    } else {
	      return false;
	    }
	  }

	  public String getName() {
	    return name;
	  }

	  public void setName(String name) {
	    this.name = name;
	  }

	  public boolean isStarted() {
	    return started;
	  }

	  public void start() {
	    started = true;
	  }

	  public void stop() {
	    started = false;
	  }
	  
	  public static void setLIMIT(int lIMIT) {
		LIMIT = lIMIT;
	}
}
