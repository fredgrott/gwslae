/**
 * Logback: the reliable, generic, fast and flexible logging framework.
 * Copyright (C) 1999-2011, QOS.ch. All rights reserved.
 *
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *
 *   or (per the licensee's choosing)
 *
 * under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation.
 */
package org.bitbucket.fredgrott.gwslae;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;

/**
 * GWSLAELayout, config looks like this:
 * 
 * <code>
 * <configuration>
 *
 *  <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
 *
 *   <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
 *    <layout class="org.bitbucket.fredgrott.gwslae.GWSLAELayout">
 *       <prefix>MyPrefix</prefix>
 *       <printThreadName>false</printThreadName>
 *    </layout>
 *   </encoder>
 * </appender>
 *
 * <root level="debug">
 *   <appender-ref ref="STDOUT" />
 * </root>
 * </configuration>
 * </code>
 * 
 * @author fredgrott
 *
 */
public class GWSLAELayout extends LayoutBase<ILoggingEvent> {

	 String prefix = null;
	  boolean printThreadName = true;

	  public void setPrefix(String prefix) {
	    this.prefix = prefix;
	  }

	  public void setPrintThreadName(boolean printThreadName) {
	    this.printThreadName = printThreadName;
	  }

	  public String doLayout(ILoggingEvent event) {
	    StringBuffer sbuf = new StringBuffer(128);
	    if (prefix != null) {
	      sbuf.append(prefix + ": ");
	    }
	    sbuf.append(event.getTimeStamp() - event.getLoggerContextVO().getBirthTime());
	    sbuf.append(" ");
	    sbuf.append(event.getLevel());
	    if (printThreadName) {
	      sbuf.append(" [");
	      sbuf.append(event.getThreadName());
	      sbuf.append("] ");
	    } else {
	      sbuf.append(" ");
	    }
	    sbuf.append(event.getLoggerName());
	    sbuf.append(" - ");
	    sbuf.append(event.getFormattedMessage());
	    sbuf.append(CoreConstants.LINE_SEPARATOR);
	    return sbuf.toString();
	  }
}
