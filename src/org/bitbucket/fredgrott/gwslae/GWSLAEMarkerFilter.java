package org.bitbucket.fredgrott.gwslae;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * <code>
 * <filter class="your.pkg.MarkerFilter">
 *   <marker>YOUR_MARKER</marker>
 *  <onMatch>ACCEPT</onMatch>
 *   <onMismatch>DENY</onMismatch>
 * </filter>
 * </code>
 * @author fredgrott
 *
 */
public class GWSLAEMarkerFilter extends AbstractMatcherFilter<ILoggingEvent> {

	 /** The marker to match. */
    private Marker markerToMatch = null;
    
    /**
     * Start.
     *
     * @see ch.qos.logback.core.filter.Filter#start()
     */
    @Override
    public void start() {
        if (null != this.markerToMatch)
            super.start();
         else
            addError("!!! no marker yet !!!");
    }
    
    /**
     * Decide.
     *
     * @param event the event
     * @return the filter reply
     * @see ch.qos.logback.core.filter.Filter#decide(java.lang.Object)
     */
    @Override
    public FilterReply decide(ILoggingEvent event) {
        Marker marker = event.getMarker();
        if (!isStarted())
            return FilterReply.NEUTRAL;
        if (null == marker)
            return onMismatch;
        if (markerToMatch.contains(marker))
            return onMatch;
        return onMismatch;
    }
    
    /**
     * Sets the marker.
     *
     * @param markerStr the new marker
     */
    public void setMarker(String markerStr) {
        if(null != markerStr)
            markerToMatch = MarkerFactory.getMarker(markerStr);
    }
}
