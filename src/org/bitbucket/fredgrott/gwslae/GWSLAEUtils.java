package org.bitbucket.fredgrott.gwslae;

import java.net.URL;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.Loader;
import ch.qos.logback.core.util.StatusPrinter;

public class GWSLAEUtils {
	
	static String XmlConfigFilePath;
	
	public void setXmlConfigFilePath(String xmlConfigFilePath) {
		XmlConfigFilePath = xmlConfigFilePath;
	}
	
	public String getXmlConfigFilePath() {
		return XmlConfigFilePath;
	}
	
	/**
	 * set the XmlConfigFilePath first and than
	 * configureViaXML_File();
	 * than you can do 
	 * Logger logger = LoggerFactory.getLogger(Our.class);
	 * 
	 * and start using..easy example is doing MDC
	 * 
	 */
	static void configureViaXML_File() {
	    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
	    try {
	      JoranConfigurator configurator = new JoranConfigurator();
	      configurator.setContext(lc);
	      lc.reset();
	      URL url = Loader.getResourceBySelfClassLoader(XmlConfigFilePath);
	      configurator.doConfigure(url);
	    } catch (JoranException je) {
	      StatusPrinter.print(lc);
	    }
	  }

}
